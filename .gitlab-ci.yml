#
# Copyright 2018 Royal Bank of Scotland
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

variables:
  MAVEN_CLI_OPTS: "-s maven/settings.xml --batch-mode -T 2C"

before_script:
- npm set registry $NPM_REPO
- npm set $NPM_CREDENTIALS

cache:
  paths:
    - /root/.m2/repository

stages:
  - build
  - snapshot_release
  - release

build:
  stage: build
  tags:
    - script-maven
  only:
    - master
    - /^[0-9]+-.*$/
  script:
    - mvn $MAVEN_CLI_OPTS clean license:check install
    - bash <(curl -s https://codecov.io/bash) -t $CODECOV_TOKEN

deploy:
  stage: snapshot_release
  tags:
    - script-maven
  script:
    - mvn $MAVEN_CLI_OPTS clean license:format dokka:javadocJar deploy -DskipTests -P release
  only:
    - master

release:
  stage: release
  tags:
    - script-maven
  variables:
    BRAID_VERSION: $CI_COMMIT_REF_NAME
  only:
    - /^v[0-9]+\.[0-9]+\.[0-9]+$/
  when: manual
  script:
    - rm -rf ~/.gnupg
    - gpg --version
    - gpg --import --batch --passphrase "$GPG_PASSPHRASE" <<< "$GPG_KEY"
    - gpg --import-ownertrust <<< "$GPG_OWNER_TRUST"
    - mvn $MAVEN_CLI_OPTS versions:set -DnewVersion=${BRAID_VERSION:1}
    - mvn $MAVEN_CLI_OPTS --batch-mode clean license:format dokka:javadocJar deploy -DskipTests -P release,npm-release
