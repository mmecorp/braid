/**
 * Copyright 2018 Royal Bank of Scotland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.bluebank.braid.core.socket

import io.bluebank.braid.core.logging.loggerFor

abstract class AbstractSocket<R, S> : Socket<R, S> {
  companion object {
    private val logger = loggerFor<AbstractSocket<*, *>>()
  }

  private val listeners = mutableListOf<SocketListener<R, S>>()

  override fun addListener(listener: SocketListener<R, S>): Socket<R, S> {
    listeners += listener
    listener.onRegister(this)
    return this
  }

  protected fun Socket<R, S>.onData(item: R): Socket<R, S> {
    listeners.forEach {
      try {
        it.dataHandler(this, item)
      } catch (err: Throwable) {
        logger.error("failed to dispatch onData", err)
      }
    }
    return this
  }

  protected fun Socket<R, S>.onEnd() {
    listeners.forEach {
      try {
        it.endHandler(this)
      } catch (err: Throwable) {
        logger.error("failed to dispatch onEnd", err)
      }
    }
  }
}